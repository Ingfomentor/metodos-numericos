#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 17:16:59 2021

@author: Angel Vázquez-Patiño
"""
#%% Example 4, page 29, Boyce, W.E., DiPrima, R.C., Meade, D.B., 2017. Elementary differential equations and boundary value problems, 11th ed. Wiley, United States of America.
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button
%matplotlib qt
def f(t, y_de_t):
    return 4*t-(2*y_de_t)/t   

def y(t):
    return t**2 + 1/t**2

a, b = 0.25, 3 # intervalo de integración
N = 3
h = (b-a)/N
u_0 = 16.0625

ts = np.linspace(a, b, N+1)
us = [u_0]

for t in ts[:-1]:
    us.append(us[-1]+h*f(t, us[-1]))

# Create the figure and the line that we will manipulate
fig, ax = plt.subplots()

plt.plot(np.linspace(a, b, 100), y(np.linspace(a, b, 100)), 'k-', label='Solución analítica $t^2+1/t^2$')
line, = plt.plot(ts, us, 'bo-', label='Aproximación')
plt.legend()
plt.title('Problema de Valores Iniciales\nMétodo de Euler Progresivo (explícito)')
plt.xlabel('t')
plt.ylabel('y')

axcolor = 'lightgoldenrodyellow'
# Make a vertically oriented slider to control the amplitude
axamp = plt.axes([0.91, 0.125, 0.0225, 0.755], facecolor=axcolor)
amp_slider = Slider(
    ax=axamp,
    label="N",
    valmin=2,
    valmax=100,
    valinit=3,
    valstep=1,
    orientation="vertical"
)

# The function to be called anytime a slider's value changes
def update(val):
    N = int(amp_slider.val)
    h = (b-a)/N
    ts = np.linspace(a, b, N+1)
    us = [u_0]
    
    for t in ts[:-1]:
        us.append(us[-1]+h*f(t, us[-1]))
    
    line.set_xdata(ts)
    line.set_ydata(us)
    fig.canvas.draw_idle()

# register the update function with each slider
amp_slider.on_changed(update)

# Create a `matplotlib.widgets.Button` to reset the sliders to initial values.
resetax = plt.axes([0.835, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')


def reset(event):
    amp_slider.reset()
    
button.on_clicked(reset)
plt.show()
#%% Euler regresivo (implícito)
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button
from sympy import Symbol, Eq, solve

%matplotlib qt
def f(t, y_de_t):
    return 4*t-(2*y_de_t)/t   

def y(t):
    return t**2 + 1/t**2

a, b = 0.25, 3 # intervalo de integración
N = 3
h = (b-a)/N
u_0 = 16.0625

ts = np.linspace(a, b, N+1)
us = [u_0]
u_n = Symbol('u_n')
for t in ts[1:]:
    res = solve(Eq(us[-1] + h*f(t, u_n), u_n), u_n)
    us.append(res[0])

# Create the figure and the line that we will manipulate
fig, ax = plt.subplots()

plt.plot(np.linspace(a, b, 100), y(np.linspace(a, b, 100)), 'k-', label='Solución analítica $t^2+1/t^2$')
line, = plt.plot(ts, us, 'bo-', label='Aproximación')
plt.legend()
plt.title('Problema de Valores Iniciales\nMétodo de Euler Regresivo (implícito)')
plt.xlabel('t')
plt.ylabel('y')

axcolor = 'lightgoldenrodyellow'
# Make a vertically oriented slider to control the amplitude
axamp = plt.axes([0.91, 0.125, 0.0225, 0.755], facecolor=axcolor)
amp_slider = Slider(
    ax=axamp,
    label="N",
    valmin=2,
    valmax=100,
    valinit=3,
    valstep=1,
    orientation="vertical"
)

# The function to be called anytime a slider's value changes
def update(val):
    N = int(amp_slider.val)
    h = (b-a)/N
    ts = np.linspace(a, b, N+1)
    us = [u_0]
    
    for t in ts[1:]:
        res = solve(Eq(us[-1] + h*f(t, u_n), u_n), u_n)
        us.append(res[0])
    
    line.set_xdata(ts)
    line.set_ydata(us)
    fig.canvas.draw_idle()

# register the update function with each slider
amp_slider.on_changed(update)

# Create a `matplotlib.widgets.Button` to reset the sliders to initial values.
resetax = plt.axes([0.835, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')


def reset(event):
    amp_slider.reset()
    
button.on_clicked(reset)
plt.show()
#%% Example 3, page 36, Boyce, W.E., DiPrima, R.C., Meade, D.B., 2017. Elementary differential equations and boundary value problems, 11th ed. Wiley, United States of America.
#from sympy import Symbol, plot_implicit, plot
#def f(x, y_de_x):
#    return (4*x-x**3)/(4+y_de_x**3)
#
#a, b = -4, 4 # intervalo de integración
#N = 40
#h = (b-a)/N
#u_0 = 4
#
#ts = np.linspace(a, b, N+1)
#us = [u_0]
#
#for t in ts[1:]:
#    us.append(us[-1]+h*f(t, us[-1]))
#
#x, y = Symbol('x'), Symbol('y')
#%matplotlib qt
#p1 = plot_implicit(y**4 + 16*y + x**4 - 8*x**2 - 448, x_var=(x, -4.5, 4.5), y_var=(y, 0, 4.5),figsize=(3, 3))
#fg, ax = p1._backend.fig, p1._backend.ax  # get matplotib's figure and axes
#ax[0].plot(ts, us, label='Aproximación', color='k')
##plt.plot(ts, ys, label='Solución analítica')
##plt.legend()
#plt.show()