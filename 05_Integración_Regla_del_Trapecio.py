from statistics import NormalDist
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button

f = NormalDist(mu=6, sigma=2)

xs = np.linspace(0, 13)
ys = np.array(list(map(f.pdf, xs)))

%matplotlib qt
a, b = 1, 10
N = 5
h = (b-a)/N
x_i = np.linspace(a, b, N+1)
f_i = list(map(f.pdf, x_i))

# Create the figure and the line that we will manipulate
fig, ax = plt.subplots()
plt.plot(xs, ys, color='whitesmoke', lw=5, label='$N(\mu=6, \sigma=2)$')
plt.legend()
plt.title('Integración Numérica\nRegla del trapecio')
stems = plt.stem(x_i, f_i, linefmt='k-')
line, = plt.plot(x_i, f_i, color='k')

axcolor = 'lightgoldenrodyellow'
# Make a vertically oriented slider to control the amplitude
axamp = plt.axes([0.91, 0.125, 0.0225, 0.755], facecolor=axcolor)
amp_slider = Slider(
    ax=axamp,
    label="N",
    valmin=1,
    valmax=50,
    valinit=5,
    valstep=1,
    orientation="vertical"
)

# The function to be called anytime a slider's value changes
def update(val):
    N = int(amp_slider.val)
    h = (b-a)/N
    x_i = np.linspace(a, b, N+1)
    f_i = list(map(f.pdf, x_i))
    stems[0].set_xdata(x_i) # 0 son los puntitos
    stems[0].set_ydata(f_i)
    stems[1].set_paths([np.array([[x, 0], [x, y]]) for (x, y) in zip(x_i, f_i)])
    stems[2].set_xdata([np.min(x_i), np.max(x_i)]) # 2 es la línea horizontal de la base de los stems
    line.set_xdata(x_i)
    line.set_ydata(f_i)
    fig.canvas.draw_idle()


# register the update function with each slider
amp_slider.on_changed(update)

# Create a `matplotlib.widgets.Button` to reset the sliders to initial values.
resetax = plt.axes([0.835, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')


def reset(event):
    amp_slider.reset()
    
button.on_clicked(reset)

plt.show()