#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  9 10:37:27 2021

@author: Angel Vázquez-Patiño
"""
metodos = ('Método del punto medio',
           'Método de Euler modificado',
           'Método de Heun')
metodo = metodos[1]
parametros = {'Método del punto medio': (1/2, 1/2, 0, 1),
              'Método de Euler modificado': (1, 1, 1/2, 1/2),
              'Método de Heun': (2/3, 2/3, 1/4, 3/4),
             }

#%% Example 4, page 29, Boyce, W.E., DiPrima, R.C., Meade, D.B., 2017. Elementary differential equations and boundary value problems, 11th ed. Wiley, United States of America.
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
%matplotlib qt
def f(t, y_de_t):
    return 4*t-(2*y_de_t)/t   

def y(t): # solución analítica
    return t**2 + 1/t**2

a, b = 0.25, 3 # intervalo de integración
N = 3
h = (b-a)/N # paso de discretización
u_0 = 16.0625

ts = np.linspace(a, b, N+1)
us = [u_0] # las aproximaciones de y

# de esto dependen los diferentes métodos de RK de 2do orden
# c_2, a_21, b1, b2 = 1/2, 1/2, 0, 1 # Método del punto medio
# c_2, a_21, b1, b2 = 1, 1, 1/2, 1/2 # Método de Euler modificado
# c_2, a_21, b_1, b_2 = 2/3, 2/3, 1/4, 3/4 # Método de Heun

c_2, a_21, b_1, b_2 = parametros[metodo]

for t in ts[:-1]: # método de RK progresivo (explícito) de segundo orden
    k1 = f(t, us[-1])
    k2 = f(t + c_2*h, us[-1] + h*a_21*k1)
    us.append(us[-1]+h*(b_1*k1 + b_2*k2))

# Create the figure and the line that we will manipulate
fig, ax = plt.subplots()

plt.plot(np.linspace(a, b, 100), y(np.linspace(a, b, 100)), 'k-', label='Solución analítica $t^2+1/t^2$')
line, = plt.plot(ts, us, 'bo-', label='Aproximación')
plt.legend()
titulo = plt.title('Problema de Valores Iniciales\n{0} (RK explícito)'.format(metodo), fontweight='bold', fontsize='xx-large')
plt.xlabel('t')
plt.ylabel('y')

axcolor = 'lightgoldenrodyellow'
# Make a vertically oriented slider to control the amplitude
axamp = plt.axes([0.91, 0.125, 0.0225, 0.755], facecolor=axcolor)
amp_slider = Slider(
    ax=axamp,
    label="N",
    valmin=2,
    valmax=100,
    valinit=3,
    valstep=1,
    orientation="vertical"
)

rax = plt.axes([0.67, 0.025, 0.15, 0.1], facecolor=axcolor)
radio = RadioButtons(rax, ('Método del punto medio',
           'Método de Euler modificado',
           'Método de Heun'), active=1)

# The function to be called anytime a slider's value changes
def update(val):
    N = int(val)
    h = (b-a)/N
    ts = np.linspace(a, b, N+1)
    us = [u_0]
    
    for t in ts[:-1]:
        k1 = f(t, us[-1])
        k2 = f(t + c_2*h, us[-1] + h*a_21*k1)
        us.append(us[-1]+h*(b_1*k1 + b_2*k2))
    
    line.set_xdata(ts)
    line.set_ydata(us)
    fig.canvas.draw_idle()
# register the update function with each slider
amp_slider.on_changed(update)

def hzfunc(label):
    c_2, a_21, b_1, b_2 = parametros[label]
    titulo.set_text('Problema de Valores Iniciales\n{0} (RK explícito)'.format(label))
    N = int(amp_slider.val)
    h = (b-a)/N
    ts = np.linspace(a, b, N+1)
    us = [u_0]
    
    for t in ts[:-1]:
        k1 = f(t, us[-1])
        k2 = f(t + c_2*h, us[-1] + h*a_21*k1)
        us.append(us[-1]+h*(b_1*k1 + b_2*k2))
    
    line.set_xdata(ts)
    line.set_ydata(us)
    fig.canvas.draw_idle()
    
radio.on_clicked(hzfunc)

# Create a `matplotlib.widgets.Button` to reset the sliders to initial values.
resetax = plt.axes([0.835, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')


def reset(event):
    radio.set_active(1)
    amp_slider.reset()
    
button.on_clicked(reset)
plt.show()